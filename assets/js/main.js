barba.init({
    transitions: [{
        name: 'pphp',
        leave: function(data) {
            var done = this.async();
            TweenMax.to(data.current.container, .5, {
                ease: Sine.easeOut,
                opacity: 0,
                onComplete: done
            });
        },
        enter: function(data) {
            var done = this.async();
            TweenMax.from(data.next.container, .5, {
                ease: Sine.easeIn,
                opacity: 0,
                onComplete: done
            });
        }
    }]
});